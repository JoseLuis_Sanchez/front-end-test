import { Component, OnInit } from "@angular/core";
import { DragulaService } from "ng2-dragula";
import { Subscription } from "rxjs";
import { GroupsService } from "../providers/groups.service";

@Component({
  selector: "app-groups",
  templateUrl: "./groups.component.html",
  styleUrls: ["./groups.component.css"],
})
export class GroupsComponent implements OnInit {
  groups: any;
  aux: any;
  newgroups: any = []  
  MANY_ITEMS = 'MANY_ITEMS';
  subs = new Subscription();

  constructor(private groupsService: GroupsService, private dragulaService:DragulaService) {
    // this.subs.add(dragulaService.dropModel(this.MANY_ITEMS)
    //   .subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
    //     console.log('soltar modelo:');
    //     // console.log(el);
    //     // console.log(source);
    //     // console.log(target);
    //     // console.log('desde ->', sourceModel);
    //     // console.log('a -> ',targetModel);
    //     console.log('item que agarra', item.id);
    //     // console.log('', this.newgroups)
    //   })
    // );
    // this.subs.add(dragulaService.removeModel(this.MANY_ITEMS)
    //   .subscribe(({ el, source, item, sourceModel }) => {
    //     console.log('removeModel:');
    //     // console.log(el);
    //     // console.log('desde ->', source);
    //     // console.log('a ->', sourceModel);
    //     console.log('remove item', item);
    //   })
    // );
  }

  ngOnInit() {
    this.getGroups();
  }
  ngOnDestroy() {
    this.subs.unsubscribe();
  }
  getGroups() {
    this.groupsService.consultaGroups().subscribe(
      (resp) => {
        // console.log(resp);
        if (resp["success"]) {
          this.aux = resp["data"]["groups"];
          this.groups = resp["data"]["groups"];
        }
      },
      (err) => {
        console.log(err.status);
      }
    );
  }
  searchItem(event) {
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.groups = this.groups.filter((item) => {
        return item.name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.groups = this.aux;
    }
  }
}
