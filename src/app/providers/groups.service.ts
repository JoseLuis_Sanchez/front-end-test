import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class GroupsService {
  constructor(private api: ApiService) {}

  consultaGroups() {
    return this.api.get("groups/jose_luis_reyes");
  }
}
