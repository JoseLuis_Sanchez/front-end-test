import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) {}
  get(path): Observable<any> {
    return this.http.get(environment.url + "/" + path);
  }
  post(path, body): Observable<any> {
    return this.http.post(environment.url + "/" + path, body);
  }
}
