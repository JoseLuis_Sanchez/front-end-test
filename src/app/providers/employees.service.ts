import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class EmployeesService {
  constructor(private api: ApiService) {}
  consultaEmployees() {
    return this.api.get("employees/jose_luis_reyes");
  }
  createEmployee(body) {
    return this.api.post("employees/jose_luis_reyes", body);
  }
  getEmployeesByGroup(id) {
    return this.api.get("employees/jose_luis_reyes/getByGroup?id=" + id);
  }
}
