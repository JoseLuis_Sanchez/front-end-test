import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { EmployeesService } from "../providers/employees.service";
declare var $;
@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.css"],
})
export class EmployeesComponent implements OnInit {
  employees: any;
  employeeForm: FormGroup;
  constructor(
    private spinner: NgxSpinnerService,
    private employeesService: EmployeesService
  ) {}

  ngOnInit() {
    this.getEmployees();
  }
  initEmployeeForm() {
    this.employeeForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(30), Validators.maxLength(30)]),
      last_name: new FormControl("", [Validators.required, Validators.minLength(30), Validators.maxLength(30)]),
      birthday: new FormControl("", [Validators.required]),
    });
  }
  getEmployees() {
    this.spinner.show();
    this.employeesService.consultaEmployees().subscribe(
      (resp) => {
        this.spinner.hide();
        // console.log(resp);
        if (resp["success"]) {
          this.employees = resp["data"];
        } else {
          this.messageAlert(
            "warning",
            "¡Lo sentimos!",
            "Por el momento no podimos procesar tu solicitud, intentalo mas tarde."
          );
        }
      },
      (err) => {
        this.spinner.hide();
        this.messageAlert("warning", "¡Lo sentimos!", "Por el momento no podimos procesar tu solicitud, intentalo mas tarde.");
        console.log(err.status);
      }
    );
  }
  searchEmployee(event) {
    const val = event.target.value;
    if (val && val.trim() != "") {
      this.employees["employees"] = this.employees["employees"].filter(
        (item) => {
          return (
            item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            item.last_name.toLowerCase().indexOf(val.toLowerCase()) > -1
          );
        }
      );
    } else {
      this.getEmployees();
    }
  }
  formatDate() {
    const date = this.employeeForm.controls["birthday"].value;
    let newvalue = date.replace(/-/g, "/");
    this.employeeForm.controls["birthday"].patchValue(newvalue);
  }
  saveEmployee() {
    if (this.employeeForm.valid) {
      this.spinner.show();
      this.employeesService.createEmployee(this.employeeForm.value).subscribe(
        (resp) => {
          this.spinner.hide();
          // console.log(resp);
          if(resp['success']){
            $('#employeeModal').modal('hide');
            this.messageAlert("success", "¡Listo!", "El registro se ha registrado exitosamente.");
            this.getEmployees();
          } else {
            this.messageAlert("warning", "¡Lo sentimos!", "Por el momento no podimos procesar tu solicitud, intentalo mas tarde.");
          }
        },
        (err) => {
          this.spinner.hide();
          console.log(err.status);
          this.messageAlert("warning", "¡Lo sentimos!", "Por el momento no podimos procesar tu solicitud, intentalo mas tarde.");
        }
      );
    }
  }
  messageAlert(type, title, text) {
    Swal.fire({
      position: "center",
      type: type,
      title: title,
      text: text,
      showConfirmButton: false,
      timer: 2000,
    } as any);
  }
}
