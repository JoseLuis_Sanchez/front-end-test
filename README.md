# AngularTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

<!-- 
Examen hecho por José Luis Reyes.

1. Clonar el proyecto.
2. cd /proyecto-name
3. nmp i
4. ng serve

Notas: Apartado GRupos incompleto, tenia y tengo la noción de como hacerlo, e intente, casi lo logro pero no quiero revasar el limite de tiempo.
Listado no. 3, CanActivate, reemplaze por un routeo a / en el componente NotFound, no es lo que solicita pero mi otra opción era crear el Guard y apuntar solo al componente 404 y el gurard hace el routing.  
-->